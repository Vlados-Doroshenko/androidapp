package com.example.vladdoroshenko.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.vladdoroshenko.adapter.UserAdapter
import com.example.vladdoroshenko.databinding.ActivityEditUserBinding
import com.example.vladdoroshenko.models.User
import com.example.vladdoroshenko.viewModel.EditUserViewModel

class EditUserActivity : AppCompatActivity() {
    private lateinit var viewModel: EditUserViewModel
    private lateinit var binding: ActivityEditUserBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityEditUserBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(this)[EditUserViewModel::class.java]

        val id = intent.extras?.getInt("id")!!
        viewModel.loadUserData(id)

        viewModel.user.observe(this) {
            binding.userEditImage.setImageResource(it.photo)
            binding.textEditUserName.setText(it.name)
            binding.textEditUserEmail.setText(it.email)
            binding.textEditUserDescription.setText(it.description)
        }

        binding.buttonUpdate.setOnClickListener {
            updateUserInfo(id)
            UserAdapter.activityFriendsFragment!!.recreate()
            finish()
        }
    }

    private fun updateUserInfo(id: Int) {
        val newUser = viewModel.user.value!!
        val newUserName = binding.textEditUserName.text.toString()
        val newEmail = binding.textEditUserEmail.text.toString()
        val newDescription = binding.textEditUserDescription.text.toString()

        val user = User(
            newUserName,
            newUser.online,
            newEmail,
            newUser.photo,
            newDescription
        )
        user.id = id
        viewModel.updateUserInfo(user)
    }
}