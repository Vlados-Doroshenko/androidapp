package com.example.vladdoroshenko.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.vladdoroshenko.R
import com.example.vladdoroshenko.adapter.UserAdapter
import com.example.vladdoroshenko.databinding.ActivityAddUserBinding
import com.example.vladdoroshenko.models.User
import com.example.vladdoroshenko.viewModel.AddUserViewModel

class AddUserActivity : AppCompatActivity() {
    private lateinit var viewModel: AddUserViewModel
    private lateinit var binding: ActivityAddUserBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityAddUserBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(this)[AddUserViewModel::class.java]

        binding.userAddImage.setImageResource(R.drawable.ic_launcher_foreground)
        binding.buttonSave.setOnClickListener {
            insertNewUser()
            UserAdapter.activityFriendsFragment!!.recreate()
            finish()
        }
    }

    private fun insertNewUser() {
        var newUserName = binding.textAddUserName.text.toString()
        if(newUserName == ""){
            newUserName = "New user"
        }
        val newEmail = binding.textAddUserEmail.text.toString()
        val newDescription = binding.textAddUserDescription.text.toString()

        val user = User(
            newUserName,
            onlineStatus[(0..4).random()],
            newEmail,
            R.drawable.ic_launcher_foreground,
            newDescription
        )
        viewModel.insertUser(user)
    }

    private val onlineStatus = listOf(
        (1..59).random().toString() + " min ago",
        "Online",
        (1..23).random().toString() + " hours ago",
        "Yesterday",
        (1..11).random().toString() + " month ago"
    )
}