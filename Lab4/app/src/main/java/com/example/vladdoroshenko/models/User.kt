package com.example.vladdoroshenko.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "users")
data class User(
    @ColumnInfo(name = "name")
    val name: String = "",
    @ColumnInfo(name = "online")
    val online: String = "",
    @ColumnInfo(name = "email")
    val email: String = "",
    @ColumnInfo(name = "photo")
    val photo: Int = 0,
    @ColumnInfo(name = "description")
    val description: String = ""
){
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}

