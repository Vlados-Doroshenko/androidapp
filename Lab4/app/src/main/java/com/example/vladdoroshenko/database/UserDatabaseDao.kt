package com.example.vladdoroshenko.database

import androidx.room.*
import com.example.vladdoroshenko.models.User

@Dao
interface UserDatabaseDao {
    @Insert(entity = User::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(user: User)

    @Update
    suspend fun update(user: User)

    @Query("SELECT * from users WHERE id = :key")
    suspend fun getId(key: Int): User?

    @Query("SELECT * FROM users")
    suspend fun getAllUsers(): List<User>

    @Query("SELECT COUNT(*) FROM users LIMIT 1")
    suspend fun getSize(): Int?

    @Query("SELECT * from users LIMIT 1")
    suspend fun listEmpty(): User?

    @Delete
    suspend fun delete(user: User)
}