package com.example.vladdoroshenko.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.example.vladdoroshenko.database.UserDatabase
import com.example.vladdoroshenko.models.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AddUserViewModel(application: Application) : AndroidViewModel(application) {

    private val database = UserDatabase.getInstance(application).userDatabaseDao

    fun insertUser(user: User) {
        viewModelScope.launch(Dispatchers.Main) {
            database.insert(user)
        }
    }
}