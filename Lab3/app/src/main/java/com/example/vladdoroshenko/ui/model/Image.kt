package com.example.vladdoroshenko.ui.model

import com.example.vladdoroshenko.ui.InterfaceAdapter

data class Image(
    val id: Int = 0,
    val name: String? = null,
    val time: String? = null,
) : InterfaceAdapter {

    override fun getType(): Int {
        return InterfaceAdapter.IMAGE_TYPE
    }
}
