package com.example.vladdoroshenko.ui.model

import com.example.vladdoroshenko.ui.InterfaceAdapter

data class User(
    val id: Int = 0,
    val name: String? = null,
//    val age: Int = 0,
    val email: String? = null,
    val status: String? = null,
//    val onlineStatus: Boolean = false
) : InterfaceAdapter {

    override fun getType(): Int {
        return InterfaceAdapter.USER_TYPE
    }
}
