package com.example.vladdoroshenko.ui

interface OnMessageClick {
    fun onClick()
    fun onLongClick()
}