package com.example.vladdoroshenko.ui.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.vladdoroshenko.ui.InterfaceAdapter
import com.example.vladdoroshenko.ui.model.Image
import com.example.vladdoroshenko.ui.model.User

class MainActivityViewModel : ViewModel() {
    private val _users = MutableLiveData<ArrayList<InterfaceAdapter>>()
    val users: LiveData<ArrayList<InterfaceAdapter>> = _users

    fun loadUserList() {
        val list = ArrayList<InterfaceAdapter>()
        list.add(User(0, "Vladyslav Doroshenko", "doroshenko.vladyslav@vu.cdu.edu.ua", "Online"))
        list.add(Image(1, "Pavlo Halo", "1 minute ago"))
        list.add(User(2, "Test Test", "test@gmail.com", "Offline"))
        _users.value = list
    }
}