package com.example.vladdoroshenko.view.repository

class ListUseCase(private val moviesListRepository: ListRepository) {
    suspend fun loadItemList() = moviesListRepository.getItemList()
}
