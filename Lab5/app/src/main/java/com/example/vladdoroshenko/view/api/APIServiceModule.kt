package com.example.vladdoroshenko.view.api

import org.koin.dsl.module

val APIServiceModule = module {
    factory { APIService().start() }
}