package com.example.vladdoroshenko.view.repository

import com.example.vladdoroshenko.view.models.Item

interface ListRepository {
    suspend fun getItemList(): List<Item>
}