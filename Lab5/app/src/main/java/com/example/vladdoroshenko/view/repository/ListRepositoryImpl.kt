package com.example.vladdoroshenko.view.repository

import com.example.vladdoroshenko.view.api.ListService

class ListRepositoryImpl(private val listService: ListService) : ListRepository {
    override suspend fun getItemList() = listService.getItems()
}