package com.example.vladdoroshenko.view.di

import com.example.vladdoroshenko.view.repository.ListRepositoryImpl
import com.example.vladdoroshenko.view.repository.ListRepository
import org.koin.dsl.module

val dataModule = module {
    single<ListRepository> {
        ListRepositoryImpl(listService = get())
    }
}