package com.example.vladdoroshenko.view.di

import com.example.vladdoroshenko.view.repository.ListUseCase
import org.koin.dsl.module

val domainModule = module {
    factory{
        ListUseCase(moviesListRepository = get())
    }
}