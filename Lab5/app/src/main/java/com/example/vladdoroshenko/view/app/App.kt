package com.example.vladdoroshenko.view.app

import android.app.Application
import com.example.vladdoroshenko.view.api.APIServiceModule
import com.example.vladdoroshenko.view.di.appModule
import com.example.vladdoroshenko.view.di.dataModule
import com.example.vladdoroshenko.view.di.domainModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger(Level.DEBUG)  // To output errors in logger
            androidContext(this@App) // Application context
            modules(listOf(APIServiceModule, appModule, domainModule, dataModule))
        }
    }
}