package com.example.vladdoroshenko.view.api

import com.example.vladdoroshenko.view.models.Item
import retrofit2.http.GET

interface ListService {
    @GET("photos")
    suspend fun getItems(): List<Item>
}